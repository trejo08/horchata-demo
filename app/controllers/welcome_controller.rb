class WelcomeController < ApplicationController
  respond_to :json
  def index
    # @greeting = Blog.all
    @greeting = 'Hello Horchaters!'
    respond_with @greeting
  end
end
